<?php

    namespace console\commands;

    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;

    class EmployeeCommand extends Command
    {
        protected function configure()
        {
            $this
                ->setName('company:employee')
                ->addArgument('profession', InputArgument::OPTIONAL);
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $arg = $input->getArgument('profession');

            switch ($arg) {
                case $arg == 'programmer':
                    $output->writeln("Programmer can write code, test code, communicate with manager");
                    break;
                case $arg == 'designer':
                    $output->writeln("Designer can draw, communicate with manager");
                    break;
                case $arg == 'tester':
                    $output->writeln("Tester can test code, communicate with manager, create tasks");
                    break;
                case $arg == 'manager':
                    $output->writeln("Manager can create tasks");
                    break;
                default:
                    $output->writeln("Choose one if this position: programmer/designer/tester/manager");
            }
        }
    }
