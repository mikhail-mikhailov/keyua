<?php

    namespace console\commands;

    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use src\Entities\Programmer;
    use src\Entities\Designer;
    use src\Entities\Tester;
    use src\Entities\Manager;

    class IsCanCommand extends Command
    {
        private $programmer;
        private $designer;
        private $tester;
        private $manager;

        public function __construct( )
        {
            parent::__construct();
            $this->programmer = new Programmer();
            $this->designer = new Designer();
            $this->tester = new Tester();
            $this->manager = new Manager();
        }

        protected function configure()
        {
            $this
                ->setName('employee:can')
                ->addArgument('profession', InputArgument::REQUIRED)
                ->addArgument('skill', InputArgument::REQUIRED);
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {

            $profession = strtolower($input->getArgument('profession'));

            if (!isset($this->$profession)) {
                die('В нашей компании нет такой должности');
            }

            $employee = $this->$profession;
            $method = $input->getArgument('skill');

            if (!method_exists($employee, $method)) {
                die('Нет такого рода занятий') ;
            }
            $skill = $employee->$method();

            $output->writeln($skill);
        }
    }
