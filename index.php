<?php

    use \Symfony\Component\Console\Application;
    use \console\commands\EmployeeCommand;
    use console\commands\IsCanCommand;

    require __DIR__ . '/vendor/autoload.php';

    $app = new Application();

    $app->add(new EmployeeCommand());
    $app->add(new IsCanCommand());

    $app->run();
