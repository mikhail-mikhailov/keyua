<?php

    namespace src\Entities;

    use src\Interfaces\EmployeeInterface;

    class Designer implements EmployeeInterface
    {
        public function writeCode()
        {
            print_r("false");
        }

        public function testCode()
        {
            print_r("false");
        }

        public function speakWithManager()
        {
            print_r("true");
        }

        public function draw()
        {
            print_r("true");
        }

        public function createTask()
        {
            print_r("false");
        }
    }
