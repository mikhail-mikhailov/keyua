<?php

    namespace src\Entities;

    use src\Interfaces\EmployeeInterface;

    class Manager implements EmployeeInterface
    {
        public function writeCode()
        {
            print_r("false");
        }

        public function testCode()
        {
            print_r("false");
        }

        public function speakWithManager()
        {
            print_r("false");
        }

        public function draw()
        {
            print_r("false");
        }

        public function createTask()
        {
            print_r("true");
        }
    }
