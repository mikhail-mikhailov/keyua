<?php

    namespace src\Entities;

    use src\Interfaces\EmployeeInterface;

    class Programmer implements EmployeeInterface
    {
        public function writeCode()
        {
           print_r("true");
        }

        public function testCode()
        {
            print_r("true");
        }

        public function speakWithManager()
        {
            print_r("true");
        }

        public function draw()
        {
            print_r("false");
        }

        public function createTask()
        {
            print_r("false");
        }
    }
