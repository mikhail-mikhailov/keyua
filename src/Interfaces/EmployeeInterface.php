<?php

    namespace src\Interfaces;

    interface EmployeeInterface
    {
        public function writeCode();
        public function testCode();
        public function speakWithManager();
        public function draw();
        public function createTask();
    }
